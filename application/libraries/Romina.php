<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class Romina{
    public function __construct(){
    $this->load->library('session');
    $this->load->library('user_agent');
    $this->load->database();
  }

  public function msg($body, $toWhere=TRUE){
    $msg = $body;
    $this->session->set_flashdata('msg2usr', $msg);
    if($toWhere==TRUE){
      redirect($toWhere);
    }
  }

  public function logthis($log,$userExterno=FALSE){
    $tabla = 'log_acceso';
    $usuario_id = $userExterno ? $userExterno : $this->session->usuario->id;
    $this->timeStamp($tabla);
    $this->db->set(
      array(
        'id_usuario'=> $usuario_id,
        'log'   => $log,
        'ip'    => $this->input->ip_address(),
        'browser' => $this->user_agent(),
        'so'    => $this->agent->platform()
      )
    );
    $this->db->insert($tabla);
  }

  public function timeStamp($table,$accion=TRUE){
    if($accion!=FALSE){
      if($this->db->field_exists('created_at', $table)){
        $this->db->set('created_at', 'NOW()', false);
      }
    }
    if($this->db->field_exists('updated_at', $table)){
      $this->db->set('updated_at', 'NOW()', false);
    }
  }

  public function fecha($fecha){
    return strftime('%d de %B de %Y', strtotime($fecha));
  }

  public function hora($fecha){
    return strftime('%d de %B de %Y', strtotime($fecha));
  }

  public function fechahora($fecha){
    return strftime('%d de %B de %Y a las %H:%M', strtotime($fecha));
  }

  private function user_agent(){
    if ($this->agent->is_browser()){
      $agent = $this->agent->browser().' - '.$this->agent->version();
    }
    elseif ($this->agent->is_robot()){
      $agent = $this->agent->robot();
    }
    elseif ($this->agent->is_mobile()){
      $agent = $this->agent->mobile();
    }
    else{
      $agent = 'Unidentified User Agent';
    }
    return $agent;
  }

  /**
     * Habilita la ejecución global de la libre CI
     * @param $var
     */
    public function __get($var){
        return get_instance()->$var;
    }
}
