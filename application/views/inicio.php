<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-screen">
   <div class="row">
      <div id="bloqueUno" class="col-md-6 txtBlack marginToSubmenu marginToSubmenuMobile">
         <div class="col-md-12 sinpd  w3-animate-left">
            <div class="col-md-12 sinpd">
               <div class="col-md-12 miniText sinpd">
                  <p class="titleCostena">¡PONLE LO DIVERTIDO!<span class="superindice">®</span><br><span class="minisubtitleCostena">CON LA COSTEÑA</span><span class="superindice">®</span></p>
                    <br>
                  <div class="txtSmall text-center txt-bold txtMobileInit">Compra $50 pesos en productos <br>La Costeña®, calcula cuántos tomatitos hay, registra tu ticket, ¡y gana uno de los 5 kits Back to School!</div>
                  <div class="col-md-6 col-xs-12 sinpd">
                    <a href="/snoopy/login" target="_self">
                      <button class="participaBtn participacion" data-analytics="Participa" ></button>
                    </a>
                </div>
                </div>
            </div>
         </div>
      </div>
      <div id="bloqueVideo" class="col-md-6 video-container w3-animate-right">
      <iframe src="https://www.youtube.com/embed/z3JAKwuTisE?rel=0&modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>
      </div>
   </div>
</div>
<div class="pleca-snoopy w3-animate-left">
  <img src='<?= base_url('assets/img/snoopy-greca.png');?>' alt="Snoopy Costeña"></img>
  <img src='<?= base_url('assets/img/charlie-greca.png');?>' alt="Snoopy Costeña"></img>
</div>
  <!--Mobile Bottles-->
  <div class="botellas-detail-mobile">
    <img id="imgUno" src="assets/img/catsup_charlie.png" alt="Snoopy Costeña"></img>
    <img src="assets/img/catsup_charlie-snoopy.png" alt="Snoopy Costeña"></img>
    <img src="assets/img/catsup_snoopy.png" alt="Snoopy Costeña"></img>
    <img src="assets/img/catsup_woodstock.png" alt="Snoopy Costeña"></img>
</div>

<!--Mobile Bottles-->
<div id="textosInicio" class="container-screen">
   <div class="row">
      <div class="col-sm-5 col-md-6 txtBlack">
          
         <div class="col-md-12 sinpd">
            <div class="col-md-12 sinpd">
               <div id="textoTomatesUno" class="col-md-12 miniText sinpd">
                  <p class="txtBig subtitlesCostena"><span class="txtNumBig">1.</span> CUENTA LOS TOMATITOS</p>
                  <div class="txtEspecial txt-bold ">Compra $50 pesos en productos<br> La Costeña® e ingresa con tu cuenta de Facebook® para calcular cuántos tomates hay dentro de la botella de Cátsup de Snoopy®</div>
                </div>

                <div id="textoTomatesDos" class="col-md-12 miniText sinpd paddingLeftLarge">
                  <p class="txtBig subtitlesCostena"><span class="txtNumBig">2.</span>  REGÍSTRATE</p>
                  <div class="txtEspecial txt-bold ">Registra tu ticket de compra.</div>
                </div>


                <div id="textoTomatesTres" class="col-md-12 miniText sinpd paddingLeftShort">
                  <p class="txtBig subtitlesCostena"><span class="txtNumBig">3.</span> COMPARTE</p>
                  <div class="txtEspecial txt-bold ">Comparte con tus amigos y se uno de los posibles ganadores de increíbles premios.</div>
                </div>

            </div>
         </div>
      </div>

      <div id="imgUnoHome" class="col-sm-5 col-sm-offset-2 col-md-6 col-md-offset-0 columns imgsecond-container">
        <img id="banderaUno" src='assets/img/snoopybandera.png' alt="Snoopy Costeña"></img>
        <img id="banderaDos" src='assets/img/woodstockbandera.png' alt="Snoopy Costeña"></img>
        <div class="col-md-12 col-xs-12 sinpd">
        <a href="/snoopy/login" target="_self">
            <button class="participaBtnb participacion" data-analytics="Participa" ></button>
        </a>
          </div>
      </div>
   </div>
</div>
   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="<?= base_url('assets/img/close.png');?>"></button>
            </div>
            <div class="modal-body text-center">
               <a href="<?= $authUrl ?>" class="participacion" data-analytics="Login Facebook"><img src="<?= base_url('assets/img/facebook.png');?>"></a>
            </div>
         </div>
      </div>
   </div>


<img src='<?= base_url('assets/img/footer-charlie.png');?>' class="footer-bases-img" alt="Snoopy Costeña"></img>



