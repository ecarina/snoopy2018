<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-screen container-screen-mobile">
   <div class="row">
    <div class="col-md-12 txtBlack marginToSubmenu">
        <div class="modal-body text-center w3-animate-left">
            <a href="<?= $authUrl ?>" class="participacion" data-analytics="Login Facebook">
                    <img src="<?= base_url('assets/img/snoopy-login.png');?>"><br>
                    <img class="btn_facebook_login" src="<?= base_url('assets/img/loging-facebook.png');?>">
            </a>
        </div>
        <div class="clear-snoopy"></div>
    </div>
    </div>
</div>
<img src='<?= base_url('assets/img/footer-charlie.png');?>' class="footer-bases-img" alt="Snoopy Costeña"></img>

  <!--Mobile-->
  <div id="pleca-mobile" class="pleca-snoopy w3-animate-left">
  <img src='<?= base_url('assets/img/snoopy-greca.png');?>' alt="Snoopy Costeña"></img>
  <img src='<?= base_url('assets/img/charlie-greca.png');?>' alt="Snoopy Costeña"></img>
</div>
  <!--Mobile Bottles-->
  <div class="botellas-detail-mobile">
    <img id="imgUno" src="assets/img/catsup_charlie.png" alt="Snoopy Costeña"></img>
    <img src="assets/img/catsup_charlie-snoopy.png" alt="Snoopy Costeña"></img>
    <img src="assets/img/catsup_snoopy.png" alt="Snoopy Costeña"></img>
    <img src="assets/img/catsup_woodstock.png" alt="Snoopy Costeña"></img>
</div>

