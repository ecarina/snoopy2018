<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="marginToSubmenu"></div>
<div class="minigame text-center w3-animate-left">
<div class="titleCostena txtLeft text-center-mobile">MINIGAMES</div><br><br>
      <div class="col-md-12 mini">
            <iframe class="minijuegoiFrame" src="<?= base_url('games/snake/index.html');?>"></iframe>
      </div>
</div>
<img src='<?= base_url('assets/img/footer-charlie.png');?>' class="footer-bases-img" alt="Snoopy Costeña"></img>
</div>
  <!--Mobile-->
  <div id="pleca-mobile" class="pleca-snoopy w3-animate-left">
  <img src='<?= base_url('assets/img/snoopy-greca.png');?>' alt="Snoopy Costeña"></img>
  <img src='<?= base_url('assets/img/charlie-greca.png');?>' alt="Snoopy Costeña"></img>
</div>
    <!--Mobile Bottles-->
  <div class="botellas-detail-mobile-cuatro">
    <img id="imgUno" src="assets/img/catsup_charlie.png" alt="Snoopy Costeña"></img>
    <img src="assets/img/catsup_charlie-snoopy.png" alt="Snoopy Costeña"></img>
    <img src="assets/img/catsup_snoopy.png" alt="Snoopy Costeña"></img>
    <img src="assets/img/catsup_woodstock.png" alt="Snoopy Costeña"></img>
</div>
  <!--Mobile Bottles-->

