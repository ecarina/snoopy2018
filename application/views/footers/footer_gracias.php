<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

</div><!-- END CONTAINER -->

<footer class="footer">
<img id="charliepapalote" src="assets/img/charlie-papalote.png" class="w3-animate-left-charlie" alt="Snoopy Costeña"></img>
   <div class="row legales">
      <div class="col-md-6 col-xs-4 come_bien">Come bien</div>
      <div class="col-md-6 col-xs-8 peanuts">©2018 Peanuts Worldwide LLC</div>
   </div>
</footer>

<script type="text/javascript" src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/app.js');?>"></script>
   </body>
      </html>
