<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

</div><!-- END CONTAINER -->

<footer class="footer">
<img id="charliepapalote" src="assets/img/charlie-papalote.png" class="w3-animate-left-charlie" alt="Snoopy Costeña"></img>
<div id="botellasBig" class="botellas-detail">
    <img id="botella-uno" src="assets/img/catsup_charlie.png" alt="Snoopy Costeña"></img>
    <img id="botella-dos"  src="assets/img/catsup_charlie-snoopy.png" alt="Snoopy Costeña"></img>
    <img id="botella-tres"  src="assets/img/catsup_snoopy.png" alt="Snoopy Costeña"></img>
    <img id="botella-cuatro"  src="assets/img/catsup_woodstock.png" alt="Snoopy Costeña"></img>
</div>
   <div class="row">
      <div class="text-center col-md-12 col-xs-12 sinpd">
      </div>
   </div>
   <div class="row legales">
      <div class="col-md-6 col-xs-4 come_bien txtBold">Come bien</div>
      <div class="col-md-6 col-xs-8 peanuts">©2018 Peanuts Worldwide LLC</div>
   </div>
   <div class="col-md-12 col-xs-12">
      <p class="privacidad">Esta página web utiliza cookies propias y de terceros para ofrecer una mejor experiencia y servicio.<span><a href="http://www.lacostena.com.mx/es/"></a></span>Al navegar aceptas el uso que hacemos de ellas.</br><span><a class="aceptarprivacidad" style="font-size: 1.5em; color: white; background-color: #DC1E35; padding: 2px 25px;">Aceptar</a></span></p>
   </div>
</footer>

<script type="text/javascript" src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/js.cookie.js');?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/app.js');?>"></script>
</body>
</html>
