<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-screen">
   <div class="row">
      <div class="col-sm-5 col-md-6 txtBlack marginToSubmenu mobile-text w3-animate-left especial-width">
         <div class="col-md-12 sinpd">
            <div class="col-md-12 sinpd">
               <div class="col-md-12 miniText sinpd">
                  <p class="titleCostena">MECÁNICA</p>
                    <br>
                  <div class="txtSmall text-center txt-bold">
                   <p><span class="txtNum">1. </span> Da clic en ¡Participa!<p><br>
                   <p><span class="txtNum">2. </span> Ingresa con tu cuenta de Facebook<p><br>
                   <p><span class="txtNum">3. </span> Sube la foto de tu ticket de compra, ingresa el número de ticket y elige la tienda<p><br>
                   <p><span class="txtNum">4. </span> Calcula cuántos tomatitos hay y da clic en "Enviar"<p><br>
                   <p><span class="txtNum">5. </span> Compártelo en Facebook<p><br>
                   <p><span class="txtNum">6. </span> Daremos a conocer los 5 ganadores el 13 de octubre de 2018<p>
                  </div>
                </div>
            </div>
         </div>
      </div>
      <div class="col-sm-5 col-sm-offset-2 col-md-6 col-md-offset-0 columns video-container w3-animate-right especial-width">
      <div class="col-md-6 col-xs-12 sinpd mecanica-img">
          <img id="snoopyuno" src='assets/img/charliesinfoco.png' class="desktop-img" alt="Snoopy Costeña"></img>
          <img id="snoopydos" src='assets/img/foco.png' class="desktop-img" alt="Snoopy Costeña"></img>
          <img id="snoopytres" src='assets/img/snoopybandera.png' class="desktop-img" alt="Snoopy Costeña"></img>
          <img id="snoopycuatro" src='assets/img/woodstockbandera.png' class="desktop-img" alt="Snoopy Costeña"></img>
          <a href="/snoopy/login" target="_self">
            <button class="participaBtn participacion participaBtnMobile" data-analytics="Participa" ></button>
          </a>
      </div>
      </div>
   </div>
</div>
<!--<div class="pleca-snoopy">
  <img src='assets/img/snoopy-greca.png' alt="Snoopy Costeña"></img>
  <img src='assets/img/charlie-greca.png' alt="Snoopy Costeña"></img>
</div>-->
  <!--Mobile-->
  <div id="pleca-mobile" class="pleca-snoopy w3-animate-left">
  <img src='<?= base_url('assets/img/snoopy-greca.png');?>' alt="Snoopy Costeña"></img>
  <img src='<?= base_url('assets/img/charlie-greca.png');?>' alt="Snoopy Costeña"></img>
</div>
  <!--Mobile Bottles-->
  <div class="botellas-detail-mobile-dos">
    <img id="imgUno" src="assets/img/catsup_charlie.png" alt="Snoopy Costeña"></img>
    <img id="imgDos" src="assets/img/catsup_charlie-snoopy.png" alt="Snoopy Costeña"></img>
    <img  id="imgTres" src="assets/img/catsup_snoopy.png" alt="Snoopy Costeña"></img>
    <img  id="imgCuatro" src="assets/img/catsup_woodstock.png" alt="Snoopy Costeña"></img>
</div>
<div class="clear-snoopy"></div>



<img src='<?= base_url('assets/img/footer-charlie.png');?>' class="footer-bases-img" alt="Snoopy Costeña"></img>