<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container-screen">
    <div class="row">
        <div class="col-md-6 marginToSubmenu w3-animate-left">
        <div id="view360" class="vista360">
                <div class="imagen360" style="position:relative;">
                    <div class="zooms">
                    <img class="imggiro" src="<?= base_url('assets/img/360-degrees.png');?>" alt="giro Snoopy"></img>
                        <button class="btnZoom" id="btnZoom"><img src="<?= base_url('assets/img/zoom-in.png');?>" width="30" height="30"></button>
                        <button class="btnnoZoom" id="btnnoZoom"><img src="<?= base_url('assets/img/zoom-out.png');?>" width="30" height="30"></button>
                    </div>
                    <div style="position:relative">           
                        <img src="<?= base_url('assets/img/360/la_costena_000.jpg'); ?>" width="480" height="320"
                            class="reel"
                            id="image"
                            data-images="<?= base_url('assets/img/360/la_costena_###.jpg|000..055'); ?>">
                    </div>
                </div>
            </div> 
        </div>
<!--Form Part One-->
    <div class="col-md-6 centrar-elemento marginToSubmenu w3-animate-right">
    <div id="form-uno">
        <div class="usaludo mobile-align title-mobile">¡Hola <?= $this->session->usuario->first_name ?>!</div><br>
            <form class="form_ticket" method="post" action="<?= site_url('snoopy/ticket') ?>" enctype="multipart/form-data">
            <div class="form-group question-how">
                <img class="botle-ticket" src='<?= base_url('assets/img/catsup_snoopy.png');?>' alt="Snoopy"></img>
                <label class="upregunta txt-bold mobile-align-two" for="botellas">¿Cuántos tomatitos hay?</label><br>
                <input id="cuantos" type="text" class="btns_ticket"  name="botellas" placeholder="0000">
                    <p class="siguienteBtn" onclick="changeForm()"></p>
                    <p id="errormessage"></p>
            </div>
    </div>
<!--Form Part Two-->
<div  id="form-dos" class="container-screen">
    <div class="row">
        <div class="col-md-12 form-contenido w3-animate-left">
        <h2 class="title-registra">Registra tu ticket</h2>
        <div class="contenedor-formulario">
                <div class="form-group">
                    <div class="inputModificado especialInput">
                        <label class="upregunta txt-bold" for="lote">Sube el ticket</label><br>
                        <input class="inputImagen" id="archivo1" name="archivo1" placeholder="imagen.jpg" />
                        <div class="botonInputFileModificado">
                          <input type="file" id="ticket" name="ticket" onchange="document.getElementById('archivo1').value = this.value.split('\\').pop().split('/').pop()"/>
                        </div>            
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="inputModificado">
                            <label class="upregunta txt-bold" for="lote">Número del ticket <span class="signo-pregunta"> &nbsp;?&nbsp;</span></label><br>
                            <input type="text" class="btns_ticket_normal" id="no_ticket" name="no_ticket" placeholder="000000000000">
                        </div>
                  </div>
                  <div class="form-group select-input">
                    <div class="inputModificado">
                        <label class="upregunta txt-bold" for="retailer">Retailer</label><br>
                        <select name="retailer" id="retailer" class="btns_ticket_normal">
                            <option value="Bodega Aurrera">Bodega Aurrera</option>
                            <option value="3B3B">3B</option>
                            <option value="Casa Ley">Casa Ley</option>
                            <option value="Chedraui">Chedraui</option>
                            <option value="Com City Fresko">Com City Fresko</option>
                            <option value="Comercial Mexicana">Comercial Mexicana</option>
                            <option value="Grupo OXXO">Grupo OXXO</option>
                            <option value="HEB">HEB</option>
                            <option value="Soriana">Soriana</option>
                            <option value="Super Neto">Super Neto</option>
                            <option value="Super precios">Super precios</option>
                            <option value="Superama">Superama</option>
                            <option value="Waldos">Waldos</option>
                            <option value="WalmartWalmart">Walmart</option>
                        </select>
                    </div>
                  </div>
                  <br>
                  <div class="form-group">
                    <div class="inputModificado text-especial-acepto"> 
                        <div class="squaredTwo">
                        <input type="checkbox" class="check_ticket" id="acepto" name="acepto" checked="checked">
                        <label for="acepto"></label>
                        </div>
                        <label class="upregunta txt-bold terminoscondiciones" for="acepto">Acepto los <a href="snoopy/bases" target="_blank" class="linkterminos">términos y condiciones</a></label>
                    </div>
                  </div>
                <br>
                  <div class="text-center">
                     <button type="submit" id="enviar_ticket" name="enviar_ticket"></button>
                  </div>
                </div>
            </form>
            </div>
            <div class="fail"><?= @$this->session->flashdata('msg2usr'); ?></div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<div class="clear-snoopy"></div>
<img src='<?= base_url('assets/img/footer-charlie.png');?>' class="footer-bases-img" alt="Snoopy Costeña"></img>