<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>¡Ponle lo divertido!®</title>

   <base href="<?=site_url('');?>">

   <link href="https://fonts.googleapis.com/css?family=Muli:300,400,900" rel="stylesheet" type="text/css">
   <link href="<?=site_url('assets/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
   <link href="<?=site_url('assets/css/style.css');?>" rel="stylesheet" type="text/css">
   <link href="<?=site_url('assets/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
   <link href="">
   <link rel="icon" href="../assets/img/favicon.ico">
   <script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
     ga('create', 'UA-24102623-1', 'auto');
     ga('send', 'pageview');
   </script>
</head>
<body>
  <!--
  Start of DoubleClick Floodlight Tag: Please do not remove
  Activity name of this tag: LaCosteña_Inicio
  URL of the webpage where the tag is expected to be placed: http://snoopy.previewsandbox.com/snoopy/inicio
  This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
  Creation Date: 08/25/2017
  -->
  <script type="text/javascript">
  var axel = Math.random() + "";
  var a = axel * 10000000000000;
  document.write('<iframe src="https://5078902.fls.doubleclick.net/activityi;src=5078902;type=lacos0;cat=lacos0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
  </script>
  <noscript>
  <iframe src="https://5078902.fls.doubleclick.net/activityi;src=5078902;type=lacos0;cat=lacos0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
  </noscript>
  <!-- End of DoubleClick Floodlight Tag: Please do not remove -->
  
   <header class="page-header">
      <div class="center-contents">
         <div>
            <a class="logo" href="#" target="_blank" title=""><img src="<?= base_url('assets/img/logo-lacostena.png');?>"></a>
            <span class="toggle-nav menu_bar"><i class="fa fa-bars fa-3x" aria-hidden="true"></i></span>
         </div>         
         <nav class="page-nav collapse">
            <ul>
               <li><a href="http://www.lacostena.com.mx/" title="" class="btn_menu">La Costeña®</a></li>
               <li><a href="http://www.lacostena.com.mx/es/productos-perfilador/" title="" class="btn_menu">Productos</a></li>
               <li><a href="http://www.lacostena.com.mx/es/home-recetas/" title="" class="btn_menu">Recetas</a></li>
               <li><a href="http://www.lacostena.com.mx/es/lata/" title="" class="btn_menu">Lata</a></li>
               <li><a href="http://www.lacostena.com.mx/es/medio-ambiente/" title="" class="btn_menu">Medio Ambiente</a></li>
               <li><a href="http://www.lacostena.com.mx/es/contacto/" title="" class="btn_menu">Contáctanos</a></li>
               <li><a href="https://www.facebook.com/lacostenamx" target="_blank" title=""><i class="fa fa-facebook redesfb" aria-hidden="true"></i></a></li>
               <li><a href="https://twitter.com/lacostenamx" target="_blank" title=""><i class="fa fa-twitter redes" aria-hidden="true"></i></a></li>
               <li><a href="https://www.pinterest.com/lacostenamexico/" target="_blank" title=""><i class="fa fa-pinterest-p redes" aria-hidden="true"></i></a></li>
               <li><a href="https://www.youtube.com/channel/UCoUzXUSoPnjPbN5MNSRdFyg/featured" target="_blank" title=""><i class="fa fa-youtube redes" aria-hidden="true"></i></a></li>
               <li><a href="http://www.lacostena.com.mx/es/" title="" class="btn_menu">ES</a></li>
               <li><a href="http://www.lacostena.com.mx/en/" title="" class="btn_menu">EN</a></li>
            </ul>
         </nav>
      </div>
   </header>

   <div class="<?= $class_container?>">
      <div class="col-md-12 nubes">
         <div class="col-md-12 col-xs-12 text-center sinpd">
            <div id="menu-desktop" class="submenu col-md-12 text-center col-xs-12 sinpd">
               <div class="row col-xs-12 sinpd" style="margin:auto;">
                  <a href="/snoopy" class="submenus" target="_self"><div class="col-md-2 col-xs-3 submenu_nav subMov"><img src="<?= base_url('assets/img/ico-inicio.png');?>"></div></a>
                  <a href="/snoopy/mecanica" class="submenus" target="_self" ><div class="col-md-2 col-xs-3 submenu_nav subMov"><img src="<?= base_url('assets/img/ico-mecanica.png');?>"></div></a>
                  <a href="/snoopy/premios" class="submenus" target="_self"><div class="col-md-2 col-xs-3 submenu_nav subMov"><img src="<?= base_url('assets/img/ico-premios.png');?>"></div></a>
                  <a href="/snoopy/bases" class="submenus"  target="_self"><div class="col-md-2 col-xs-3 submenu_nav subMov"><img src="<?= base_url('assets/img/ico-bases.png');?>"></div></a>
                  <a href="/snoopy/minigames" class="submenus"  target="_self"><div class="col-md-2 col-xs-3 submenu_nav subMov"><img src="<?= base_url('assets/img/ico-minigames.png');?>"></div></a>
               </div>
            </div>
         </div>
         <div id="menu-mobile" class="submenu col-md-12 text-center col-xs-12 sinpd">
               <div class="row col-xs-12 sinpd" style="margin:auto;">
                  <a href="/snoopy" class="submenus" target="_self"><div class="col-md-2 col-xs-3 submenu_nav subMov"><img src="<?= base_url('assets/img/icomobile-inicio.png');?>"></div></a>
                  <a href="/snoopy/mecanica" class="submenus" target="_self" ><div class="col-md-2 col-xs-3 submenu_nav subMov"><img src="<?= base_url('assets/img/icomobile-mecanica.png');?>"></div></a>
                  <a href="/snoopy/premios" class="submenus" target="_self"><div class="col-md-2 col-xs-3 submenu_nav subMov"><img src="<?= base_url('assets/img/icomobile-premios.png');?>"></div></a>
                  <a href="/snoopy/bases" class="submenus"  target="_self"><div class="col-md-2 col-xs-3 submenu_nav subMov"><img src="<?= base_url('assets/img/icomobile-bases.png');?>"></div></a>
                  <a href="/snoopy/minigames" class="submenus"  target="_self"><div class="col-md-2 col-xs-3 submenu_nav subMov"><img src="<?= base_url('assets/img/icomobile-minigames.png');?>"></div></a>
               </div>
            </div>
      </div>
