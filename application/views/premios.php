<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-screen">
   <div class="row">
      <div class="col-sm-5 col-md-6 txtBlack marginToSubmenu w3-animate-left especial-width">
         <div class="col-md-12 sinpd">
            <div class="col-md-12 sinpd">
               <div class="col-md-12 miniText sinpd">
                  <p class="titleCostena">PREMIOS</p>
                    <br>
                  <div class="txtSmall text-center txt-bold text-center-mobile">
                   <p>Calcula cuántos tomatitos y sé uno de los afortunados ganadores de un kit escolar y algunos de estos premios:</p><br>
                  </div><br>
                  <div class="txtSmallEspecial text-center txt-bold cuadrado-especial">
                    <p> 2 primeros lugares: Computadora Lenovo <br>
                        2 segundos lugares: Tableta Samsung <br>
                        3 terceros lugares: Reloj Ghia
                    </p><br>
                  </div>
                </div>
            </div>
         </div>
      </div>
      <div class="col-sm-5 col-sm-offset-2 col-md-6 col-md-offset-0 columns video-container w3-animate-right especial-width">
      <div class="col-md-6 col-xs-12 sinpd">
          <img id="img-premios" src='assets/img/premios-snoopy.png' alt="Snoopy Costeña"></img>
          <a href="/snoopy/login" target="_self">
            <button class="participaBtn participacion" data-analytics="Participa" ></button>
          </a>
      </div>
      </div>
   </div>
</div>
<!--<div class="pleca-snoopy">
  <img src='assets/img/snoopy-greca.png' alt="Snoopy Costeña"></img>
  <img src='assets/img/charlie-greca.png' alt="Snoopy Costeña"></img>
</div>-->
<!--Mobile Bottles
<div class="botellas-detail-mobile-dos">
    <img id="imgUno" src="assets/img/catsup_charlie.png" alt="Snoopy Costeña"></img>
    <img src="assets/img/catsup_charlie-snoopy.png" alt="Snoopy Costeña"></img>
    <img src="assets/img/catsup_snoopy.png" alt="Snoopy Costeña"></img>
    <img src="assets/img/catsup_woodstock.png" alt="Snoopy Costeña"></img>
</div>-->
<div class="clear-snoopy"></div>


<img src='<?= base_url('assets/img/footer-charlie.png');?>' class="footer-bases-img" alt="Snoopy Costeña"></img>