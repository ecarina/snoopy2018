var cuantos ='';
/*var totalBotle = document.getElementById("totalBotle");*/

$(document).ready(function(){
// DECLARACIÓN DE LA FUNCION

function toggleNavigation(){
	$('.page-header').toggleClass('menu-expanded');
	$('.page-nav').toggleClass('collapse');
}

	// EVENTOS DEL DOM
	$(window).on('load',function(){
		$('.toggle-nav').click(toggleNavigation);
	});


	$('#btn-light').on('click', '[data-toggle="lightbox"]', function(event) {
		event.preventDefault();
		$(this).ekkoLightbox();
	});


	$('#btnZoom').on('click',function(event){
		$(".imagen360").removeClass("nozoom");
		$(".imagen360").addClass("zoom");
	});

	$('#btnnoZoom').on('click',function(event){
		$(".imagen360").removeClass("zoom");
		$(".imagen360").addClass("nozoom");
	});


	$('#btnZoomMov').on('click',function(event){
		$(".reelMov").removeClass("nozoom");
		$(".reelMov").addClass("zoom");
	});

	$('#btnnoZoomMov').on('click',function(event){
		$(".reelMov").removeClass("zoom");
		$(".reelMov").addClass("nozoom");
	});

	$(".participacion").on('click',function(e){
		ga('send', {
			hitType: 'event',
			eventCategory: 'Participación',
			eventAction: 'click',
			eventLabel: $(this).attr('data-analytics')
		});
	});

	$('.aceptarprivacidad').on('click',function(){
		Cookies.set('cookietime','1');
		$('.privacidad').fadeOut('slow');
	});
	$('.privacidad').delay(8000).fadeOut('slow');
    $('.aceptarprivacidad').on('click',function(e){
 	$('.privacidad').hide();
 });

 $('.privacidad').show();
	/*if(typeof Cookies.get('cookietime') === 'undefined'){
		console.log('nohaycookie');
		$('.privacidad').show();
	} else {
		$('.privacidad').hide();
	}*/


/* Select Menu*/
var loc = window.location.href;
var submenu = document.getElementsByClassName("submenus");
for (var x = 0; x< submenu.length ; x++ ){
	if(submenu[x].href === loc) {
		submenu[x].firstChild.classList.add("selected");
	}
}

});

/*Change Form */

function changeForm() {
	cuantos = document.getElementById('cuantos').value;
	/*localStorage.setItem("botellass", cuantos);*/
	if(cuantos === '' || cuantos === undefined)
	 {
		document.getElementById('errormessage').innerHTML = 'Recuerda llenar cuántos tomatitos hay';
	 } else {
		document.getElementById('form-uno').style.display ='none';
		document.getElementById('view360').style.display ='none';
		document.getElementById('form-dos').style.display ='block';
	 }

}


/*if (totalBotle === null || totalBotle === undefined) { }else {

document.getElementById("totalBotle").innerHTML = localStorage.getItem("botellass");
}*/

/*Animation Scroll*/
var w = window.innerWidth;
if (w > 375) {
if (window.location.pathname === '/snoopy' || window.location.pathname === '/snoopy/' ) {
	document.addEventListener('scroll', function (event) {
		var bloqueUno = document.getElementById('bloqueUno');
		var bloqueVideo = document.getElementById('bloqueVideo');
		var textoTomatesUno = document.getElementById('textoTomatesUno');
		var textoTomatesDos = document.getElementById('textoTomatesDos');
		var textoTomatesTres = document.getElementById('textoTomatesTres');
		var imgUnoHome = document.getElementById('imgUnoHome');

		if(window.scrollY === 0 ) {
			bloqueUno.style.display='block';
			bloqueVideo.style.display='block';
			textoTomatesUno.style.display='none';
			textoTomatesDos.style.display='none';
			textoTomatesTres.style.display='none';
			imgUnoHome.style.display='none';
			bloqueUno.classList.add("animate-left");
			bloqueVideo.classList.add("animate-right");
			textoTomatesUno.classList.remove("animate-left");
			textoTomatesDos.classList.remove("animate-left-two");
			textoTomatesTres.classList.remove("animate-left-three");
			imgUnoHome.classList.remove("animate-right");
		} 
		else if (window.scrollY > 400 ) {
			textoTomatesUno.style.display='block';
			textoTomatesDos.style.display='block';
			textoTomatesTres.style.display='block';
			imgUnoHome.style.display='block';
			bloqueUno.classList.remove("animate-left");
			bloqueVideo.classList.remove("animate-right");
			textoTomatesUno.classList.add("animate-left");
			textoTomatesDos.classList.add("animate-left-two");
			textoTomatesTres.classList.add("animate-left-three");
			imgUnoHome.classList.add("animate-right");
		} 
		if (window.scrollY > 500 ) {
			bloqueUno.style.display='none';
			bloqueVideo.style.display='none';
		}
	});
}
}

/*if (w > 769) {
document.addEventListener('scroll', function (event) { 
	var botellasBig = document.getElementById('botellasBig');
	if (window.location.pathname === '/snoopy' || window.location.pathname === '/snoopy/' ) {
			if(window.scrollY > 720) {
				botellasBig.style.display='block';
				botellasBig.classList.add("fadeInUp");
			} else {
				botellasBig.style.display='none';
				botellasBig.classList.remove("fadeInUp");
			}
	} 	
if (window.location.pathname === '/snoopy/mecanica' || window.location.pathname === '/snoopy/mecanica/' 
	|| window.location.pathname === '/snoopy/premios') {
		if(window.scrollY > 397) {
			botellasBig.style.display='block';
			botellasBig.classList.add("fadeInUp");
		} else {
			botellasBig.style.display='none';
			botellasBig.classList.remove("fadeInUp");
		}
	}
	if (window.location.pathname === '/snoopy/bases' ){
		if(window.scrollY > 3100) {
			botellasBig.style.display='block';
			botellasBig.classList.add("fadeInUp");
		} else {
			botellasBig.style.display='none';
			botellasBig.classList.remove("fadeInUp");
		}
	}
	if (window.location.pathname === '/snoopy/minigames' ){
		if(window.scrollY > 150) {
			botellasBig.style.display='block';
			botellasBig.classList.add("fadeInUp");
		} else {
			botellasBig.style.display='none';
			botellasBig.classList.remove("fadeInUp");
		}
	}
});
}*/