<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="backPasto">
    <div class="vista360">
        <div class="imagen360" style="position:relative;">
            <div class="zooms">
                    <button class="btnZoom" id="btnZoom"><img src="<?= base_url('assets/img/zoom_mas.png');?>" width="30" height="30"></button>
                    <button class="btnnoZoom" id="btnnoZoom"><img src="<?= base_url('assets/img/zoom_menos.png');?>" width="30" height="30"></button>
                </div>
            <div style="position:relative">     
                <img src="<?= base_url('assets/img/360/la_costena_000.jpg'); ?>" width="480" height="320"
                    class="reel"
                    id="image"
                    data-images="<?= base_url('assets/img/360/la_costena_###.jpg|000..055'); ?>">
            </div>
        </div>
    </div>
</div>

<div class="vista360Mov">
    <div style="position:relative">
        <img src="<?= base_url('assets/img/360/la_costena_000.jpg'); ?>" width="480" height="320"
                class="reel reelMov"
                id="image"
                data-images="<?= base_url('assets/img/360/la_costena_###.jpg|000..055'); ?>">
        
        <div class="zoomsMov">
            <button class="btnZoomMov" id="btnZoomMov"><img src="<?= base_url('assets/img/zoom_mas.png');?>" width="30" height="30"></button>
            <button class="btnnoZoomMov" id="btnnoZoomMov"><img src="<?= base_url('assets/img/zoom_menos.png');?>" width="30" height="30"></button>
        </div>
    </div>
</div>
