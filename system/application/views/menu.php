<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="submenu col-md-6">
   <div class="row">
      <div class="col-md-1 submenu_nav"><img src="<?= base_url('assets/img/menu_mini_games');?>"></div>
      <div class="col-md-1 submenu_nav"><img src="<?= base_url('assets/img/menu_mecanica');?>"></div>
      <div class="col-md-1 submenu_nav"><img src="<?= base_url('assets/img/menu_premios');?>"></div>
      <div class="col-md-1 submenu_nav"><img src="<?= base_url('assets/img/menu_bases');?>"></div>
   </div>
</div>
<div class="col-md-6"></div>
