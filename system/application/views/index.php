<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container">
   <div class="snoopy_loader">
      <div class="snoopybaby">
         <a href="<?= site_url('snoopy/inicio');?>">
         <img src="<?= base_url('assets/img/loader.png');?>">
         <div class="wod"><img src="<?= base_url('assets/img/gif.gif');?>"></div>
         </a>
      </div>
   </div>
</div>
