<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

</div><!-- END CONTAINER -->

<footer class="footer">
   <div class="row">
      <div class="text-center col-md-12 col-xs-12 sinpd">
         <div class="col-md-6 col-xs-12 sinpd">
            <button class="participaBtn participacion" data-toggle="modal" data-target="#myModal" data-analytics="Participa" 
            ></button>
         </div>
      </div>
   </div>
   <div class="row legales">

      <div class="col-md-6 col-xs-4 come_bien">Come bien</div>
      <div class="col-md-6 col-xs-8 peanuts">©2018 Peanuts Worldwide LLC</div>
   </div>
   <div class="col-md-12 col-xs-12">
      <p class="privacidad">Esta página web utiliza cookies propias y de terceros para ofrecer una mejor experiencia y servicio.<span><a href="http://www.lacostena.com.mx/es/"></a></span>Al navegar aceptas el uso que hacemos de ellas.</br><span><a class="aceptarprivacidad" style="font-size: 1.5em; color: white; background-color: #DC1E35; padding: 2px 25px;">Aceptar</a></span></p>
   </div>
</footer>

<script type="text/javascript" src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/js.cookie.js');?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/app.js');?>"></script>
</body>
</html>
