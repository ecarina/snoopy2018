<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

</div><!-- END CONTAINER -->

<footer class="footer_calcula">
   <div class="row catsup col-xs-12 sinpd col-md-12">
      <div class="col-md-12 formCatsup col-xs-12">
         <div class="col-md-2 col-xs-2 botellita">
            <img src="<?= base_url('assets/img//botellita.png');?>">
         </div>
         <div class="col-md-7 col-xs-7">
            <div class="usaludo">¡Hola <?= $this->session->usuario->first_name ?>!</div>
            <div class="upregunta"><strong>¿Cuántas cátsup La Costeña<span class="superindice">®</span> hay?</strong></div>
            <div class="fail"><?= @$this->session->flashdata('msg2usr'); ?></div>
         </div>
         <!-- <div class="col-md-2 col-xs-3 divResp">
            <input type="text" name="btnrespuesta" id="btnrespuesta" placeholder="Escribe tu respuesta">
         </div> -->
         <div class="col-md-2 col-xs-2 divEnviar">
            <button name="¡participa!" class="btnEnviar participacion" data-toggle="modal" data-target="#ModalTicket" data-analytics="Calcula"></button>
         </div>
      </div>
   </div>

   <div class="row legales">
      <div class="col-md-6 col-xs-4 come_bien">Come bien</div>
      <div class="col-md-6 col-xs-8 peanuts">©2018 Peanuts Worldwide LLC</div>
   </div>
</footer>

<script type="text/javascript" src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/jquery.reel-min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/app.js');?>"></script>
   </body>
      </html>
