<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Snoopy extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('snoopy_model','model');
		$this->load->library('facebook');
		$this->load->library('romina');

		date_default_timezone_set('America/Mexico_City');
		setlocale(LC_TIME,'es_ES');
	}

	public function index(){
		$this->load->view('headers/header_loader');
		$this->load->view('index');
		$this->load->view('footers/footer_loader');
	}

	public function inicio(){
		// $this->session->unset_userdata('usuario');
		// $this->facebook->destroy_session();
		// $this->output->enable_profiler(TRUE);
		$data['authUrl'] = $this->facebook->login_url();
		$class['class_container']='container';
		$this->load->view('headers/header',$class);
		$this->load->view('inicio',$data);
		$this->load->view('textos');
		$this->load->view('footers/footer');
	}

	public function calcula(){
	if(!$this->session->usuario || $this->session->usuario->id == ''){
			redirect(site_url());
		}
		$class['class_container']='container_calcula';
		$this->load->view('headers/header',$class);
		$this->load->view('calcula');
		$this->load->view('textos');
		$this->load->view('footers/footer');
	}

	public function gracias(){
		$data['authUrl'] = $this->facebook->login_url();
		$class['class_container']='container_calcula';
		$this->load->view('headers/header',$class);
		$this->load->view('gracias',$data);
		$this->load->view('textos');
		$this->load->view('footers/footer_gracias');
	}
	public function authentication(){
		$userData = array();
		$response = $this->facebook->is_authenticated();

		if(isset($response['error'])){
				$this->romina->msg('Necesitamos que des acceso a tu Facebook para poder particpar', site_url('snoopy/inicio'), FALSE);
		} else {
			$userProfile = $this->facebook->request('get','/me?fields=id,first_name,last_name,email,picture.width(300)');

			if(isset($userProfile['id'])){
				$userData = $this->prepareData($userProfile, 'facebook');
				if(!$this->checkUser($userData, 'facebook')){
					$this->romina->msg('Ocurrió un error al validar tu cuenta', site_url('snoopy/inicio'), FALSE);
				}
			} else {
				$this->facebook->destroy_session();
				$this->romina->msg('Ocurrió un error al validar tu cuenta', site_url('snoopy/inicio'), FALSE);
			}
		}
	}
	public function ticket(){
		if($this->session->usuario->id==''){
			redirect(site_url());
		}
		if($this->input->post('no_ticket')!='' && $this->input->post('acepto')){
			$ticket = str_replace('#TC', '', trim($this->input->post('no_ticket')));

			$existeParticipacion = $this->model->onlyGet('participacion',array('retailer'=>$this->input->post('retailer'),'no_ticket'=>$ticket));

			if($existeParticipacion==FALSE){
				$folderUsuario='/uploads/'.$this->session->usuario->id.'/'.uniqid().'/';
				if(!is_dir('.'.$folderUsuario)){
					mkdir('.'.$folderUsuario,0777,TRUE);
				}

				$config['upload_path']      = '.'.$folderUsuario;
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = 1024;
        $config['max_width']        = 0;
        $config['max_height']       = 0;
        $config['file_ext_tolower']	= TRUE;
        $config['file_name']				= uniqid();

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('ticket')){
					$errorUpload = array('error' => $this->upload->display_errors());
					$this->romina->msg('Ocurrio un error al guardar el ticket.<br/>'.$errorUpload['error'],site_url('snoopy/calcula'));
				} else {
					$foto = array('upload_data' => $this->upload->data());
					$data = array(
						'botellas' => $this->input->post('botellas'),
						'no_ticket' => $ticket,
						'retailer' => $this->input->post('retailer'),
						'pathtoticket' => $foto['upload_data']['full_path'],
						'usuario_id' => $this->session->usuario->id
						);

					$this->model->onlyInsert('participacion',$data);
					$this->session->set_flashdata('botellas',$this->input->post('botellas'));

					redirect(site_url('snoopy/gracias/'));
				}
			} else {
					$this->romina->msg('El ticket ya fue registrado previamente',site_url('snoopy/calcula'));
			}
		} else {
					$this->romina->msg('Todos los campos son requeridos, incluyendo la aceptación de términos y condiciones',site_url('snoopy/calcula'));
		}
	}

	/**
	 * PRIVATE FUNCTIONS
	 */

	private function prepareData($userProfile, $plattform){

		$who = 'u'.uniqid().'m';

		$pw = $plattform == 'facebook' ? md5($who) : md5($userProfile['password']);
		$id =  $plattform == 'facebook' ? $userProfile['id'] : $who;
		$foto = $plattform == 'facebook' ? $userProfile['picture']['data']['url'] : base_url('profile/'.$who.'/profilepic/');
		$profileUrl = $plattform == 'facebook' ? 'https://www.facebook.com/'.$userProfile['id'] : base_url('profile/'.$who);


		$userData['nombre']				=	$userProfile['first_name'].' '.$userProfile['last_name'];
		$userData['correo']				=	$userProfile['email'];
		$userData['picture_url']	=	$foto;
		$userData['passw']				= 	$pw;
		$userData['oauth_provider']	=	$plattform;
		$userData['oauth_uid']		=	$id;
		$userData['profile_url']	=	$profileUrl;
		$userData['first_name'] = $userProfile['first_name'];
  	$userData['last_name'] = $userProfile['last_name'];

		return $userData;
	}

	private function checkUser($user, $plattform){
		$search = array('oauth_provider'=>$user['oauth_provider']);

		if($plattform=='facebook'){
			$search['oauth_uid'] = $user['oauth_uid'];
		} else {
			$search['correo'] = $user['correo'];
		}

		$userInDb = $this->model->onlyGet('usuario', $search);
		if($userInDb){
			$userInDb = $userInDb[0];
		}

		if(!$userInDb) {
			$preDB = (object) $user;
			$userInDb = $preDB;
		}

		if($plattform=='facebook'){
			$this->checkFacebookLogin($userInDb);
		} else {
			$this->checkTraditionalLogin($userInDb);
		}
		return true;
	}

	private function checkFacebookLogin($userInDb){
		if(@$userInDb->id){
			$this->updateUser(get_object_vars($userInDb));
		} else {
			$userInDb->id = $this->registerUser(get_object_vars($userInDb));
		}
		$this->goInside($userInDb);
		return true;
	}

	private function checkTraditionalLogin(){
		return false;
	}

	private function goInside($userInDb){
		$this->session->usuario = $userInDb;
		$this->romina->logthis('Inicio de sesión');
		redirect('snoopy/calcula');
	}

	private function registerUser($user){
		$userId = $this->model->onlyInsert('usuario',$user);
		if($userId){
			$this->romina->logthis('Registro del usuario', $userId);
			return $userId;
		} else {
			return FALSE;
		}
	}

	private function updateUser($user){
		return $this->model->onlyUpdate('usuario',$user,array('oauth_uid'=>$user['oauth_uid']));
	}

}
