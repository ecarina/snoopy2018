<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="backPasto">
    <div class="vista360">
        <div class="imagen360" style="position:relative;">
            <div class="zooms">
                    <button class="btnZoom" id="btnZoom"><img src="<?= base_url('assets/img/zoom_mas.png');?>" width="30" height="30"></button>
                    <button class="btnnoZoom" id="btnnoZoom"><img src="<?= base_url('assets/img/zoom_menos.png');?>" width="30" height="30"></button>
                </div>
            <div style="position:relative">
                
                <img src="<?= base_url('assets/img/360/la_costena_000.jpg'); ?>" width="480" height="320"
                    class="reel"
                    id="image"
                    data-images="<?= base_url('assets/img/360/la_costena_###.jpg|000..055'); ?>">
            </div>
        </div>
    </div>
</div>

<div class="vista360Mov">
    <div style="position:relative">
        <img src="<?= base_url('assets/img/360/la_costena_000.jpg'); ?>" width="480" height="320"
                class="reel reelMov"
                id="image"
                data-images="<?= base_url('assets/img/360/la_costena_###.jpg|000..055'); ?>">
        
        <div class="zoomsMov">
            <button class="btnZoomMov" id="btnZoomMov"><img src="<?= base_url('assets/img/zoom_mas.png');?>" width="30" height="30"></button>
            <button class="btnnoZoomMov" id="btnnoZoomMov"><img src="<?= base_url('assets/img/zoom_menos.png');?>" width="30" height="30"></button>
        </div>
    </div>
</div>



<div class="modal fade" id="ModalTicket" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="<?= base_url('assets/img/close.png');?>"></button>
         </div>
         <div class="modal-body text-center">
            <form class="form_ticket" method="post" action="<?= site_url('snoopy/ticket') ?>" enctype="multipart/form-data">
               <div class="col-md-3 col-xs-1">
                  <div class="img_ticket_snoopy">
                     <img src="<?= base_url('assets/img/snoopy_ticket.png');?>">
                  </div>
               </div>
               <div class="col-md-6 col-xs-10">
                  <div class="form-group">
                     <div class="inputModificado">
                        <label for="lote">Subir ticket</label>
                         <input class="inputImagen" id="archivo1" name="archivo1" placeholder="imagen.jpg" />
                         <div class="botonInputFileModificado">
                             <input type="file" class="inputImagenOculto" id="ticket" name="ticket"onchange="document.getElementById('archivo1').value = this.value.split('\\').pop().split('/').pop()"/>
                         </div>
                        
                     </div>
                  </div>

                  <div class="form-group">
                     <label for="lote">Número del ticket</label>
                     <input type="text" class="btns_ticket" id="no_ticket" name="no_ticket" placeholder="000000000000">
                  </div>

                  <div class="form-group">
                     <label for="retailer">Retailer</label>
                     <select name="retailer" id="retailer" class="btns_ticket">
                        <option value="Bodega Aurrera">Bodega Aurrera</option>
                        <option value="3B3B">3B</option>
                        <option value="Casa Ley">Casa Ley</option>
                        <option value="Chedraui">Chedraui</option>
                        <option value="Com City Fresko">Com City Fresko</option>
                        <option value="Comercial Mexicana">Comercial Mexicana</option>
                        <option value="Grupo OXXO">Grupo OXXO</option>
                        <option value="HEB">HEB</option>
                        <option value="Soriana">Soriana</option>
                        <option value="Super Neto">Super Neto</option>
                        <option value="Super precios">Super precios</option>
                        <option value="Superama">Superama</option>
                        <option value="Waldos">Waldos</option>
                        <option value="WalmartWalmart">Walmart</option>
                     </select>
                  </div>

                  <div class="form-group">
                     <label for="botellas">¿Cuántas catsup La Costeña ® hay?</label>
                     <input type="text" class="btns_ticket" id="botellas" name="botellas">
                  </div>

                  <div class="form-group">
                     <label for="acepto">Acepto términos y condiciones</label>
                     <input type="checkbox" class="check_ticket" id="acepto" name="acepto">
                  </div>

                  <div class="text-center">
                     <button type="submit" id="enviar_ticket" name="enviar_ticket"></button>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="img_ticket_botella">
                     <img src="<?= base_url('assets/img/botella_ticket.png');?>">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
