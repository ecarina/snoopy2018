<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>¡Ponle lo divertido!®</title>

   <base href="<?=site_url('');?>">

   <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash|Laila:400,600,700|Lato" rel="stylesheet">
   <link href="<?=site_url('assets/css/style.css');?>" rel="stylesheet" type="text/css">
   <link rel="icon" href="../assets/img/favicon.ico">
   <script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-24102623-1', 'auto');
     ga('send', 'pageview');

   </script>

</head>
<body class="loader">

