<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>¡Ponle lo divertido!</title>

   <base href="<?=site_url('');?>">

   <link href="https://fonts.googleapis.com/css?family=Muli:300,400,900" rel="stylesheet" type="text/css">
   <link href="<?=site_url('assets/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
   <link href="<?=site_url('assets/css/style.css');?>" rel="stylesheet" type="text/css">
   <link href="<?=site_url('assets/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
   <link href="">
   <link rel="icon" href="../assets/img/favicon.ico">
   <script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-81581706-8', 'auto');
     ga('send', 'pageview');

   </script>
</head>
<body>

   <header class="page-header">
      <div class="center-contents">
         <div>
            <a class="logo" href="#" title=""><img src="<?= base_url('assets/img/logo-lacostena.png');?>"></a>
            <span class="toggle-nav menu_bar"><i class="fa fa-bars fa-3x" aria-hidden="true"></i></span>
         </div>         
         <nav class="page-nav collapse">
            <ul>
               <li><a href="#" title="" class="btn_menu">La Costeña®</a></li>
               <li><a href="http://www.lacostena.com.mx/es/productos-perfilador/" title="" class="btn_menu">Productos</a></li>
               <li><a href="http://www.lacostena.com.mx/es/home-recetas/" title="" class="btn_menu">Recetas</a></li>
               <li><a href="http://www.lacostena.com.mx/es/lata/" title="" class="btn_menu">Lata</a></li>
               <li><a href="http://www.lacostena.com.mx/es/medio-ambiente/" title="" class="btn_menu">Medio Ambiente</a></li>
               <li><a href="http://www.lacostena.com.mx/es/contacto/" title="" class="btn_menu">Contáctanos</a></li>
               <li><a href="https://www.facebook.com/lacostenamx" target="_blank" title=""><i class="fa fa-facebook redesfb" aria-hidden="true"></i></a></li>
               <li><a href="https://twitter.com/lacostenamx" target="_blank" title=""><i class="fa fa-twitter redes" aria-hidden="true"></i></a></li>
               <li><a href="https://www.pinterest.com/lacostenamexico/" target="_blank" title=""><i class="fa fa-pinterest-p redes" aria-hidden="true"></i></a></li>
               <li><a href="https://www.youtube.com/channel/UCoUzXUSoPnjPbN5MNSRdFyg/featured" target="_blank" title=""><i class="fa fa-youtube redes" aria-hidden="true"></i></a></li>
               <li><a href="http://www.lacostena.com.mx/es/" title="" class="btn_menu">ES</a></li>
               <li><a href="http://www.lacostena.com.mx/en/" title="" class="btn_menu">EN</a></li>
            </ul>
         </nav>
      </div>
   </header>
   <div class="<?= $class_container?>">
      <div class="nubes">
         <div class="col-md-12 col-xs-12 text-center">
            <div class="submenu_games col-md-12 text-center col-xs-12">
               <div class="row col-xs-12">
                  <div class="col-md-2 col-xs-6 submenu_nav subMov">
                  <a href="JavaScript:window.close()" style="border: none;"><img src="<?= base_url('assets/img/menu_concurso.png');?>"></a>
                  </div>
                  <div class="col-md-2 col-xs-6 submenu_nav subMov"><a href="<?= site_url('snoopy/minigames');?>" style="border: none;"><img src="<?= base_url('assets/img/menu_mini_games.png');?>"></a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   
      
