<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.10&appId=<?= $this->config->item('facebook_app_id') ?>";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="backPasto">
   <div class="vista360">
      <div class="picPerfil">
         <img class="graciasPerfil" src="<?= $this->session->usuario->picture_url; ?>">
         <div class="text-center textoGracias">
            <strong>Calculaste <?=  $this->session->flashdata('botellas'); ?>  botellas, ahora espera los resultados y <br>
            ¡compártelo con tus amigos! </strong>
         </div>
         <div class="fb-share-button" data-href="<?= site_url(); ?>" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= urlencode(site_url()); ?>&amp;src=sdkpreparse" onclick="trackOutboundLink('<?= site_url(); ?>'); return false;">Compartir</a></div>
      </div>

   </div>
</div>

<div class="vista360Mov picPerfil">
    <img class="graciasPerfil" src="<?= $this->session->usuario->picture_url; ?>">
    <div class="text-center textoGracias">
      <strong>Calculaste <?=  $this->session->flashdata('botellas'); ?> botellas, ahora espera los resultados y <br>
      ¡compártelo con tus amigos! </strong>
      
         <div class="fb-share-button" data-href="<?= site_url(); ?>" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= urlencode(site_url()); ?>&amp;src=sdkpreparse" onclick="trackOutboundLink('<?= site_url(); ?>'); return false;">Compartir</a></div>
   </div>
</div>




