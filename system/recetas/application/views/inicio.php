<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
   <div class="row">
      <div class="col-md-7 txtRed">
         <div class="col-md-12 sinpd">
            <div class="col-md-12 col-xs-12 text-center sinpd">
               <div class="col-md-12 sinpd">
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/817VLbX37VI?rel=0" frameborder="0" allowfullscreen></iframe>
               </div>
            </div>

            <div class="col-md-12 sinpd">
               <div class="col-md-12 col-xs-12 miniText sinpd">
                  <p class="txtBig text-center">Este año los buenos conocedores </br>son los invitados de honor.</p>
                  <hr>

                  <div class="txtSmall text-center">Compra $150 pesos en productos La costeña</div>
                  <div class="txtSmall text-center">
                     <button class="participaBtn participacion" data-toggle="modal" data-target="#myModal" data-analytics="Participa">¡Participa!</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-5 columns col-xs-12">
         <div class="casaSnoopy col-xs-12"><img src="<?= base_url('assets/img/casita.png');?>" width="100%"></div>
      </div>
   </div>

   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="<?= base_url('assets/img/close.png');?>"></button>
            </div>
            <div class="modal-body text-center">
               <a href="<?= $authUrl ?>" class="participacion" data-analytics="Login Facebook"><img src="<?= base_url('assets/img/facebook.png');?>"></a>
            </div>
         </div>
      </div>
   </div>

