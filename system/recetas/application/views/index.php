<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container_loader">
   <div class="snoopy_loader">
      <div class="snoopybaby">
         <a href="<?= site_url('snoopy/inicio');?>">
         <img src="<?= base_url('assets/img/loader.png');?>">
         <div class="wod"><img src="<?= base_url('assets/img/gif.gif');?>"></div>
         </a>
         <p class="txtBig text-center miniText" style="font-family: 'Berkshire Swash', cursive;">Este año los buenos conocedores </br>son los invitados de honor.</p>
      </div>
   </div>
</div>
