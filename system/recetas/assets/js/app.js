// DECLARACIÓN DE LA FUNCION

function toggleNavigation(){
	$('.page-header').toggleClass('menu-expanded');
	$('.page-nav').toggleClass('collapse');
}

// EVENTOS DEL DOM
$(window).on('load',function(){
	$('.toggle-nav').click(toggleNavigation);
});


$('#btn-light').on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});


$('#btnZoom').on('click',function(event){
	$(".imagen360").removeClass("nozoom");
	$(".imagen360").addClass("zoom");
});

$('#btnnoZoom').on('click',function(event){
	$(".imagen360").removeClass("zoom");
	$(".imagen360").addClass("nozoom");
});


$('#btnZoomMov').on('click',function(event){
	$(".reelMov").removeClass("nozoom");
	$(".reelMov").addClass("zoom");
});

$('#btnnoZoomMov').on('click',function(event){
	$(".reelMov").removeClass("zoom");
	$(".reelMov").addClass("nozoom");
});

